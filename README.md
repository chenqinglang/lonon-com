# lononCOM
```
该项目是一个串口助手硬件资料。
支持烧录设备：
```
|      | 芯片品牌                       |
| ---- | ----------------------------- |
| 1    | espressif                     | 
| 2    | freqchip                      | 


<img src=./doc/image/image(1).jpg width=90% />

```
3D 打印外壳。
```

<img src=./doc/image/image(2).png width=90% />

<img src=./doc/image/image(3).png width=90% />

<img src=./doc/image/image(4).png width=90% />

```
组装实物。
```

<img src=./doc/image/image(5).jpg width=90% />

<img src=./doc/image/image(6).jpg width=90% />

```
双串口版本，CP2105。
```

<img src=./doc/image/image(7).jpg width=90% />